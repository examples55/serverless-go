package datalayer

import (
	"github.com/h2non/gock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetAllSpells(t *testing.T) {
	spells, err := GetAllSpells()

	assert.NoError(t, err, "request should not return an error")
	assert.NotEmpty(t, spells, "spells should not be empty")
}

func TestGetAllSpellsHttpError(t *testing.T) {
	defer gock.Off()

	gock.New("https://hp-api.herokuapp.com").
		Get("/api/spells").
		Reply(500)

	_, err := GetAllSpells()

	assert.Error(t, err, "server response of 500 should return an error")
}
