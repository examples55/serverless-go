package datalayer

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
)

type Spell struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func GetAllSpells() ([]Spell, error) {
	req, err := http.NewRequest("GET", "https://hp-api.herokuapp.com/api/spells", nil)

	if err != nil {
		return nil, err
	}

	res, err := (&http.Client{}).Do(req)

	if err != nil {
		return nil, err
	}

	if res.StatusCode != 200 {
		return nil, errors.New("failed making http request")
	}

	var spells []Spell

	body, _ := io.ReadAll(res.Body)
	err = json.Unmarshal(body, &spells)

	return spells, err
}
