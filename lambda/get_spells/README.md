# get_spells lambda function

This is the lambda function that handles the API Endpoint `/spells`. When a GET opeation is performed
on the API at this endpoint the handler function will fetch a list of spells and return the data.

This code uses the AWS Lambda function helper for Go which has documentation located [here](https://docs.aws.amazon.com/lambda/latest/dg/golang-handler.html).

In this example, we also use a local package called `datalayer`. We import this package like this:

```go
import "gitlab.com/examples55/serverless-go/pkg/datalayer"
```

