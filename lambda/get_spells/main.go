package main

import (
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/examples55/serverless-go/pkg/datalayer"
	"go.uber.org/zap"
)

// Spells structure defined to allow the handler to be a receiver function. It also allows
// "dependency injection"-ish approach to dependencies that need to be mocked for tests.
type Spells struct {
	log *zap.Logger
}

// handler is what AWS calls when the API endpoint is hit. In this case, see the
// GetSpellsFunction defined in the template.yaml. That block of yaml tells AWS to
// map this handler function to the API Gateway Endpoint -> GET /spells. The CodeUri
// points to lambda/get_spells/. This telegraphs to the AWS SAM CLI that APIGW needs
// to invoke this executable (lambda function) when a request is made.  This handler function
// will process the request and return a result. In this case we return a slice of Spell structs.
// https://docs.aws.amazon.com/lambda/latest/dg/golang-handler.html
func (sp *Spells) handler(ctx context.Context, request events.APIGatewayProxyRequest) ([]datalayer.Spell, error) {
	sp.log.Info("handler called",
		zap.Reflect("context", ctx),
		zap.Reflect("request", request))

	spells, err := datalayer.GetAllSpells()

	if err != nil {
		sp.log.Error("failed getting spells", zap.Error(err))
	}

	return spells, err
}

func main() {
	logConfig := zap.NewDevelopmentConfig()
	logConfig.Encoding = "json"
	logger, _ := logConfig.Build()

	spells := Spells{log: logger}
	lambda.Start(spells.handler)
}
