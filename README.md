# serverless-go

An exploration into and opinions about building a serverless API in AWS using Go. 

## Why?

Go already has a set of opinions and a well-defined [standard project layout](https://github.com/golang-standards/project-layout) that is very well documented. 
Why bother with yet another opinion on project layout? 

Serverless is different. Well, slightly different so these opinions are more of a minor adjustment to the
golang standards already defined.

## Directory Structure

Much like the [standard project layout](https://github.com/golang-standards/project-layout) this project has 
`go.mod` and `go.sum` files in the project root.  

`/lambda`

Lambda function handlers. This directory has a similar purpose to the [/cmd](https://github.com/golang-standards/project-layout#cmd) 
directory in command line applications however the executable operates slightly different. Since this type of executable
is bespoke to the way AWS Lambda Functions work I suggest that we do not put them in a **/cmd** directory but rather
they qualify for their own type of directory. The **cmd** directory telegraphs a CLI or executable program with a specific
set of expectations. Lambda functions have their own set of expectations.  For this reason they get their own directory.

`/pkg`

Library code that's ok to use by external applications. In this example we have a small data layer package that fetches
data from a third party API and returns a slice of well typed structures.

Reference: [https://github.com/golang-standards/project-layout/tree/master/pkg#pkg](https://github.com/golang-standards/project-layout/tree/master/pkg#pkg)

## CICD (gitlab)

This project uses Gitlab for the SCM and leverages the CICD tooling that Gitlab supplies in order to build and deploy
the code to AWS.

Reference: [https://aws.amazon.com/blogs/apn/using-gitlab-ci-cd-pipeline-to-deploy-aws-sam-applications/](https://aws.amazon.com/blogs/apn/using-gitlab-ci-cd-pipeline-to-deploy-aws-sam-applications/)


## Ways to test

### sam local (start-api)

`sam local start-api` will start your aws gateway api locally and
allow you to make requests to your lambda functions.

```shell
GOOS=linux go build -o bootstrap .
```

Then hit the api from postman.

### sam local (invoke)

`sam local invoke GetSpellsFunction` will execute the lambda function
in a container.

### build unit tests

In my opinion one of the easier ways to test is to build unit and integration tests. Check out the simple unit
test with mocked api calls in the `pkg/datalayer/datalayer_test.go`.

For mocking HTTP requests to the third party API [gock](https://github.com/h2non/gock) was used.

```go
func TestGetAllSpellsHttpError(t *testing.T) {
	defer gock.Off()

	gock.New("https://hp-api.herokuapp.com").
		Get("/api/spells").
		Reply(500)

	_, err := GetAllSpells()

	assert.Error(t, err, "server response of 500 should return an error")
}
```

**Interfaces for AWS SDK Unit Testing**

[How to video explaining creating mock interfaces for the AWS SDK](https://www.youtube.com/watch?v=dFY2hsBiFcI)

[AWS Developer Tools Blog on Mocking AWS SDK](https://aws.amazon.com/blogs/developer/mocking-out-then-aws-sdk-for-go-for-unit-testing/)

[AWS Documentation on Mocking SDK v2](https://aws.github.io/aws-sdk-go-v2/docs/unit-testing/)

## Additional documentation links.

[AWS Lambda Function Event Format and Response Format](https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-develop-integrations-lambda.html)

[Deconstructing AWS Lambda Functions](https://medium.com/build-succeeded/deconstructing-aws-lambda-functions-d1597dd054cd?sk=dbca78e5b38aecb9177b11a6b18cfcb1)